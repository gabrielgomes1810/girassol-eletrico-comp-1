#include <SPI.h>
#include <SD.h>

File Arquivo_T,Arquivo_C,Arquivo_P;

int superio_esquerdo;
int superio_direito;
int inferior_esquerdo;
int inferior_direito;
int tempo = 1;
int w=0,t=0,p=0;

int TAN=15;
double TAM = 15.0;

int cronometro=0;

//Ultimos 10 resultados da corrente, tensão e potência; //Implementação do grupo do Daniel
float tensao_corrente[2][10];
float potencia_[10];

////////////////////////////////////////////////////////
//Struct com todos os dados
typedef struct
{
double corrente, *sc = &corrente ;
double tensao, *st = &tensao ;
double potencia;
}dados;
dados resultado;

////////////////////////////////////////////////////////
//Enum dos motores. A coodernada de inicialização.

enum motores {Servo1 = 1050,Servo2 = 4000};

////////////////////////////////////////////////////////
//Função para medir a tensão
void funcaot(double *n)
{ int i;

   *n = 0;
    
   for(i=0 ; i<TAN ; i++)
  { 
    
    if(analogRead(A5)>448.2) //Fator de correção se não houver tensão.
    *n += (((analogRead(A5)-448.2)/101.3));
    else 
    *n += 0;
    
    delay(tempo);
    
  }  
   *n = (*n/TAM);
  
      if(t<10)
      {
       tensao_corrente[0][t] =  (*n);
       t++;
      }else t=0;

      
    Serial.print("\nTensão: ");
    Serial.print(*n, DEC);
    delay(tempo);

}

////////////////////////////////////////////////////////
//Função para medir a corrente
void funcaoc(double *q)
{int i;

   *q =0;

  for(i=0 ; i<TAN ; i++)
  {
    if(analogRead(A4)>890.0) //Fator de correção, caso não haja corrente.
    *q += ((analogRead(A4)-890.0));
    else
    *q += 0; 
      
    delay(tempo);
    
  }
    *q = ((*q/TAM)/13100.6);
    
      if(w<10)
      {
       tensao_corrente[1][w] = (*q);
       w++;
      }else w=0;

      

   Serial.print("\nCorrente: "); 
   Serial.println(*q, DEC);
   delay(tempo);

}
////////////////////////////////////////////////////////

void setup() {

  Serial.begin(9600);
  if (!SD.begin(4)) {
    Serial.println("Falha ao iniciar o cartão");
    while (1);
  }
  
  digitalWrite(6,HIGH);
  digitalWrite(7,HIGH);
  digitalWrite(3,HIGH);
  digitalWrite(2,HIGH);
  
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  
  TCCR1A = 0;
  TCCR1A = (1 << COM1A1) | (1 << COM1B1) | (1 << WGM11);
  TCCR1B = 0;
  TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS11);
  ICR1 = 40000;
  OCR1A = Servo1;
  OCR1B = Servo2;
}

void loop() {
  
    //Exibir a Tensão 
  funcaot(resultado.st);
  delay(tempo);
  
  //Exibir a Corrente
  funcaoc(resultado.sc);
  delay(tempo);
  
  //Exibir a Pontencia 
  resultado.potencia = (resultado.tensao*resultado.corrente); 
  
  if(p<10)
  {
    potencia_[p] = resultado.potencia;
    p++;
  }else p=0;
  
  Serial.print("Potência: ");
  Serial.print(resultado.potencia);
  Serial.println();
  delay(tempo);
////////////////////////////////////////////////////////
  //Gravação dos dados
  
  if(cronometro == 450) // Mais ou menos 30 segundos. 
  { 
   Arquivo_T = SD.open("Tensao.txt", FILE_WRITE);
   Arquivo_C = SD.open("Corrente.txt", FILE_WRITE);
   Arquivo_P = SD.open("Potencia.txt", FILE_WRITE);
   
    if (Arquivo_T && Arquivo_C && Arquivo_P) {
    Arquivo_T.println(resultado.tensao);
    Arquivo_C.println(resultado.corrente);
    Arquivo_P.println(resultado.potencia);
    Arquivo_T.close();
     Arquivo_C.close();
      Arquivo_P.close();
      
    Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Dados gravado com sucesso!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }
  else {
    Serial.println("Erro ao abrir Arquivos");
  }
 
  cronometro = 0;
  }
  delay(tempo);
  
  Serial.println(cronometro);
  cronometro += 1;
////////////////////////////////////////////////////////
 
  //Rastraemento do Sol
  superio_esquerdo = analogRead(A0);
  superio_direito = analogRead(A1);
  inferior_esquerdo = analogRead(A2);
  inferior_direito = analogRead(A3);

 
  
  
 if (superio_esquerdo > superio_direito) {
    OCR1A += 1;
    delay(tempo);
  }
  if (inferior_esquerdo > inferior_direito) {
    OCR1A += 1;
    delay(tempo);
  }
  if (superio_esquerdo < superio_direito) {
    OCR1A -= 1;
    delay(tempo);
  }
  if (inferior_esquerdo < inferior_direito) {
    OCR1A -= 1;
    delay(tempo);
  }
  if (OCR1A > 2700) {
    OCR1A = 2700;
  }
  if (OCR1A < 1050) {
    OCR1A = 1050;
  }
  if (superio_esquerdo > inferior_esquerdo) {
    OCR1B -= 1;
    delay(tempo);
  }
  if (superio_direito > inferior_direito) {
    OCR1B -= 1;
    delay(tempo);
  }
  if (superio_esquerdo < inferior_esquerdo) {
    OCR1B += 1;
    delay(tempo);
  }
  if (superio_direito < inferior_direito) {
    OCR1B += 1;
    delay(tempo);
  }
  if (OCR1B > 4000) {
    OCR1B = 4000;
  }
  if (OCR1B < 3000) {
    OCR1B = 3000;
  } 

}